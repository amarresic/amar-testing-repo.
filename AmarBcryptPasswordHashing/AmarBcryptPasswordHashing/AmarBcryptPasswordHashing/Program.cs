﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Web.Helpers;


namespace AmarBcryptPasswordHashing
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hashed password: $2a$10$EmAB9ExODQaRrERl2onz2u8PEkQy.P711cxyeOQh1jG0VpLSxWBNq
            //That is what  you need to crack - the salt is hidden in the hash - i dont need to store the salt - nor pass it when i try to match password.
            //But to be nice it is: $2a$10$EmAB9ExODQaRrERl2onz2u

            string plainText = String.Empty;
            SHA256 sha256Hasher = SHA256Managed.Create();
            byte[] plainTextBytes;
            byte[] hashByteArr;
            string salt = BCrypt.GenerateSalt(10);
            string sha256String;

            Stopwatch hashCreateTimer = new Stopwatch();
            TimeSpan hashCreateTimeused = TimeSpan.Zero;

            Stopwatch hashCheckTimer = new Stopwatch();
            TimeSpan hashCheckTimeused = TimeSpan.Zero;

            Console.WriteLine("Please enter a password:");
            plainText = Console.ReadLine();

            //This is where we are hasing the password
            hashCreateTimer.Start();
            //first off we want sha256
            sha256String = Crypto.SHA256(plainText);
            //10 is the default n of iterations for bcrypt

            string hashedPassword = BCrypt.HashPassword(sha256String, salt);
            hashCreateTimer.Stop();
            hashCreateTimeused = hashCreateTimer.Elapsed;

            Console.WriteLine(" ");

            Console.WriteLine("Bcrypt - Hash: " + hashedPassword);
            Console.WriteLine("SHA256 - Hash: " + sha256String);
            Console.WriteLine("Hashing password took " + hashCreateTimeused.TotalSeconds + " seconds");

            Console.WriteLine(" ");

            Console.WriteLine("Great, enter it again so we can try to match it:");
            plainText = Console.ReadLine();

            Console.WriteLine(" ");

            //This is where we try to match the input with the hash
            hashCheckTimer.Start();
            //gonna want sha256 again first
            sha256String = Crypto.SHA256(plainText);
            //then run it past bcrypt
            bool passwordMatch = BCrypt.CheckPassword(sha256String, hashedPassword);
            hashCheckTimer.Stop();
            hashCheckTimeused = hashCheckTimer.Elapsed; 

            Console.WriteLine(" ");

            Console.WriteLine("Password Match: " + passwordMatch);
            Console.WriteLine("Password check took " + hashCheckTimeused.TotalSeconds + " seconds");

            Console.ReadLine();
        }
    }
}
